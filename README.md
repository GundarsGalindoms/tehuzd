# README #

How to set up & run application Tehniskais Uzdevums

### What is this repository for? ###

This repository is for running Symfony web application in Docker container.

It contains:

* Docker file for nginx
* Docker file for php
* docker-compose file for building the container (includes instructions for setting up MySql)
* preconfigured MySql database for Symfony application in directory mysql
* Symfony application in directory symfony

### Prerequisities ###

Docker and Docker-Compose should be installed and started on your host.

### Setup ###

Download source to your project directory e.g. /myapp

Check file docker-compose.yml for port mapping on which web application will run. The file is provided with port 8012. You can change it if the port is already in use in your environment.

From command line cd to your project directory.

Run command to build container: 

```bash
$ docker-compose up -d --build
```

You should see following result at the end:

```bash
Starting myapp_mysql_1   ... done
Starting myapp_nginx_1 ... done
Starting myapp_php_1     ... done
```

Check if web application is available on http://localhost:8012

p.s. sometimes nginx does not start first time and webpage does not open on given port. To solve this stop containers:

```bash
$ docker-compose stop
```

In file docker-compose.yml change the port from 8012 to different one, e.g 8013 in and rebuild container with command:

```bash
$ docker-compose up -d --build
```

Now check the webpage on changed port http://localhost:8013. Webpage should be displayed.

Login to your php container to load data to database

```bash
$ docker exec -it  myapp_php_1 bash
```

Run comman to load data to database

```bash

$ php bin/console load-rates

```

Refresh the page http://localhost:port to see the result


### MYSQL access ###

You can access the db from command line using command:

```bash

$ docker exec -it myapp_mysql_1 mysql -u syuser -p

```

password: sypass
