<?php


namespace App\Service;

use App\Entity\ExchangeRate;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ExchangeRateRepository;

class ExchangeRateService
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var ExchangeRateRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $manager,ExchangeRateRepository $repository)
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    
    public function store(ExchangeRate $exchangeRate ){
        try{
            $this->manager ->persist($exchangeRate);
            $this->manager ->flush();
        }catch (\Exception $e){
            trigger_error('Something went wrong! '.$e->getMessage());
        }
    }

    public function deletoOld(){
        try{
            $this->repository->deleteAllRates();
        }catch (\Exception $e){
            trigger_error('Something went wrong when deleting rates! '.$e->getMessage());
        }
    }
}
