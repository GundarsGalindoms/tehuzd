<?php


namespace App\Controller;


use App\Repository\ExchangeRateRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeratesController extends AbstractController
{

    /**
     * @Route("/", name="app_home")
     */

     public function index(ExchangeRateRepository $repository, PaginatorInterface $paginator, Request $request){
        $query = $repository->findLatestRates();
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            20 /*limit per page*/
        );

        //$todayDate = new \DateTime();
        $latestDate = $repository->findLatestDate();

        return $this->render('exchangerate/index.html.twig',[
            'pagination' => $pagination,
            'longNames' => $this->getLongNames(),
            'latestDate' => $latestDate ? $latestDate[0]['1'] : ''
        ]);
    }

    /**
     * @Route("/rate/{id}")
     */
    public function show($id, ExchangeRateRepository $repository){

        $currAndRate = $repository->find($id);
        if($currAndRate){
            $histRates = $repository->findHistoricalRatesByRate($currAndRate->getShortName(), $currAndRate->getDate());
        }else{
            return $this->redirectToRoute('app_home');
        }

        return $this->render('exchangerate/show.html.twig',[
            'rate' => 'Showing the rate with id:'.$id,
            'longNames' => $this->getLongNames(),
            'currAndRate' => $currAndRate,
            'histRates' => $histRates
        ]);
    }

    public function getLongNames(){
        return array(
            "AUD" => "Australia, Dollars",
            "BGN" => "Bulgaria, Leva",
            "BRL" => "Brazil, Real",
            "CAD" => "Canada, Dollars",
            "CHF" => "Switzerland, Francs",
            "CNY" => "China, Yuan Renminbi",
            "CZK" => "Czech Republic, Koruny",
            "DKK" => "Denmark, Kroner",
            "GBP" => "United Kingdom, Pounds",
            "HKD" => "Hong Kong, Dollars",
            "HRK" => "Croatia, Kuna",
            "HUF" => "Hungary, Forint",
            "IDR" => "Indonesia, Rupiah",
            "ILS" => "Israel, New Shekels",
            "INR" => "India, Rupees",
            "ISK" => "Iceland, Kronur",
            "JPY" => "Japan, Yen",
            "KRW" => "Korea (South), Won",
            "MXN" => "Mexico, Peso",
            "MYR" => "Malaysia, Ringgit",
            "NOK" => "Norway, Krone",
            "NZD" => "New Zealand, Dollars",
            "PHP" => "Philippines, Peso",
            "PLN" => "Poland, Zlotych",
            "RON" => "Romania, New Lei",
            "RUB" => "Russia, Rubles",
            "SEK" => "Sweden, Kronor",
            "SGD" => "Singapore, Dollars",
            "THB" => "Thailand, Baht",
            "TRY" => "Turkey, Lira",
            "USD" => "United States of America, Dollars",
            "ZAR" => "South Africa, Rand"
        );
    }

}