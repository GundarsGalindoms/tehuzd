<?php

namespace App\Command;

use App\Service\ExchangeRateService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\ExchangeRate;

class LoadRatesCommand extends Command
{
    protected static $defaultName = 'load-rates';

    private $exchangeRateService;
    public function __construct(ExchangeRateService $exchangeRateService)
    {
        $this->exchangeRateService = $exchangeRateService;
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        // delete old rates
        $this->exchangeRateService->deletoOld();
        $output->writeln('Old records deleted.');

        // read xml and load to db
        $rss = simplexml_load_file('https://www.bank.lv/vk/ecb_rss.xml');
        foreach($rss->channel->item as $newsItem){
            $allRates = explode(" ", trim($newsItem->description, " "));
            $even = $odd = array();
            foreach($allRates as $k => $v )  $k % 2  ?  $odd[] = $v  :  $even[] = $v;

            for ($i=0; $i<=count($even); $i++){
                if(!empty($even[$i])){
                    $exRate = new ExchangeRate();
                    $exRate->setDate(\DateTime::createFromFormat(\DateTimeInterface::RSS, $newsItem->pubDate));
                    $exRate->setShortName($even[$i]);
                    $exRate->setRate($odd[$i]);
                    $exRate->setCreatedAt(new \DateTime());
                    $this->exchangeRateService->store($exRate);
                }
            }
        }
        $io->success('Exchange rates loaded!');
        return 0;
    }
}
