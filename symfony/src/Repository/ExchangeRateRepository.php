<?php

namespace App\Repository;

use App\Entity\ExchangeRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
/**
 * @method ExchangeRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExchangeRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExchangeRate[]    findAll()
 * @method ExchangeRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExchangeRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExchangeRate::class);
    }

    public function findLatestRates() : Query{

        // select * from exchange_rate where date = (select max(date) from exchange_rate);
        $dql = 'SELECT rat FROM App\Entity\Exchangerate rat WHERE rat.date = (SELECT MAX(ret.date) FROM App\Entity\Exchangerate ret)';
        $query = $this->getEntityManager()->createQuery($dql);
        //var_dump($query->getSQL());die;
        return $query;
    }

    public function findLatestDate(){
        $dql = 'SELECT MAX(ret.date) FROM App\Entity\Exchangerate ret';
        $query = $this->getEntityManager()->createQuery($dql);
        return $query->getResult();
    }

    public function findHistoricalRatesByRate($shortName, $latestDate){
        // select * from exchange_rate where short_name='AUD' order by date desc;
        $dql = 'SELECT rat FROM App\Entity\Exchangerate rat WHERE rat.short_name = :name AND rat.date != :date ORDER BY rat.date DESC';
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('name', $shortName);
        $query->setParameter('date', $latestDate);
        return $query->getResult();
    }

    public function deleteAllRates(){
        $dql = 'DELETE App\Entity\Exchangerate ret';
        $query = $this->getEntityManager()->createQuery($dql);
        $query->execute();
    }

    // /**
    //  * @return ExchangeRate[] Returns an array of ExchangeRate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExchangeRate
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
